# KeepEnInput

#### 介绍
对特定窗口自动切换为 **En** 输入模式（防止中文输入法干扰输入，如：游戏和代码编辑）  
（在切换窗口时生效）

使用样例：
搜狗输入法 `Ctrl+Space`用于切换中英文  
 **Apex**  `Ctrl+Space`用于蹲跳  
两者冲突 导致 **Apex** 惨死沙场  

#### 界面一览
![主界面](%E9%A2%84%E8%A7%88%E5%9B%BE%E4%B8%BB%E7%95%8C%E9%9D%A2.png)

#### 使用说明
双击可编辑条目  
设 条目中的文字 为`str`   
有两种模式：   
- 检测窗口标题是否包含`str`(`str`中不包含.exe，如：Visual Studio)  
- 窗口是否属于`str`这个可执行文件(str为可执行文件名，如：cmd.exe)  

会自动根据是否包含.exe来切换模式  

#### 启动项
建议手动设置启动项  
Win10如何手动添加开机启动项 - 云+社区 - 腾讯云  
https://cloud.tencent.com/developer/article/1543563